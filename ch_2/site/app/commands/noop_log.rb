# Made this class to test enqueing delayed jobs that 
# aren't ActiveRecords.
class NoopLog
  def initialize
  end

  def execute
    Rails.logger.debug "I did finally run"
  end

  handle_asynchronously :execute
end
