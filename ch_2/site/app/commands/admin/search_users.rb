class Admin::SearchUsers
  attr_reader :results, :criteria, :error

  # _criteria should be a hash.  Supported keys:
  #   email - an email address
  def initialize(_criteria)
    @criteria = _criteria || {}
  end

  def execute
    res = ::User

    if criteria[:email]
      res = res.where(:email => criteria[:email])
    end

    @results = res

    nil
  end
end
