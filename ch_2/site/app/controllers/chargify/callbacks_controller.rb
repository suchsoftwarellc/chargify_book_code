class Chargify::CallbacksController < ApplicationController
  def signup
    puts "chargify signup"
    flash[:notice] = "Thank you for signing up! We'll contact you shortly to schedule your service."
    redirect_to root_path
  end

  def account_update
    puts "chargify account_update"
    flash[:notice] = "You've successfully updated your account."
    redirect_to root_path
  end
end
