module Admins
  class UsersController < AdminsRootController
    before_filter :set_nav

    def index
      command = Admin::SearchUsers.new(params[:user_search])

      @users = 
        if !command.execute
          command.results.order(:email).page params[:page]
        else
          flash.now[:notice] = "There was a problem with your search."
          []
        end
    end

    def show
      @user = User.find(params[:id])
    end

    def impersonate
      sign_in(:user, User.find(params[:id]), { :bypass => true })
      Log.masquerade(current_admin.id,params[:id])
      redirect_to '/'
    end

    # This is for resending the confirmation instructions.  Sometimes these get lost.
    def resend_confirmation
      @user = User.find(params[:id])
      @user.resend_confirmation_instructions
      Log.resent_confirmation(current_admin.id,@user.id)
      flash[:notice] = "Confirmation instructions resent."
      redirect_to admins_user_path(:id => @user.id)
    end

    def confirm
      @user = User.find(params[:id])
      @user.confirm!
      Log.confirmed_user(current_admin.id,@user.id)
      flash[:notice] = "User confirmed."
      redirect_to admins_user_path(:id => @user.id)
    end

    def reset_password
      @user = User.find(params[:id])
      @user.send_reset_password_instructions
      Log.reset_password(current_admin.id,@user.id)
      flash[:notice] = "Password reset instructions sent."
      redirect_to admins_user_path(:id => @user.id)
    end

    private

    def set_nav
      @top_nav = 'users'
    end
  end
end
