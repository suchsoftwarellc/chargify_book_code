module Admins
  class LogsController < AdminsRootController
    before_filter :set_nav

    def index
      @logs = Log.order('created_at DESC').page(params[:page]).per(50)
    end

    private

    def set_nav
      @top_nav = 'logs'
    end
  end
end
