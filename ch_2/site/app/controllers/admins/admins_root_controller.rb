module Admins
  class AdminsRootController < ApplicationController
    layout 'admin'
    before_filter :authenticate_admin!
  end
end
