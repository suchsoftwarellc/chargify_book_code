module Admins
  class QueueController < AdminsRootController
    before_filter :set_nav

    def index
      @report = QueueHealth::DelayedJob.status
      @jobs   = ::DelayedJob.page(params[:page])
    end

    private

    def set_nav
      @top_nav = 'queue'
    end
  end
end
