class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :store_location

  # This route exists as an easy way to trigger errors for testing purposes
  def goodbye_world
    3/0
  end

  protected

  def after_sign_out_path_for(resource_or_scope)
    if :admin == resource_or_scope
      reset_session
      '/admins'
    else # assuming it was a user
      if admin_signed_in?
        admins_user_path(:id => current_user.id)
      else
        '/'
      end
    end
  end

  def store_location
    # store last url - this is needed for post-login redirect to whatever the user last visited.
    if (request.fullpath != "/users/sign_in" &&
        request.fullpath != "/users/sign_up" &&
        request.fullpath != "/users/password" &&
        !request.xhr?) # don't store ajax calls
      session[:previous_url] = request.fullpath 
    end
  end
    
  def after_sign_in_path_for(resource)
    if resource.is_a?(Admin)
      '/admins'
    else
      root_path
    end
  end
end
