module Admins
  module QueueHealth
    class DelayedJob
      def self.status
        {
          :count               => ::DelayedJob.count,
          :errors_in_queue     => ::DelayedJob.where('failed_at IS NOT NULL').count,
          :oldest_job_in_queue => ::DelayedJob.order('created_at ASC').limit(1).first,
        }
      end
    end
  end
end
