require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module FtwLawnCare
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    
    config.autoload_paths += %W(#{config.root}/commands)

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      :user_name      => ENV['SMTP_USERNAME'],
      :password       => ENV['SMTP_PASSWORD'],
      :domain         => ENV['SMTP_DOMAIN'],
      :address        => ENV['SMTP_ADDRESS'],
      :port           => ENV['SMTP_PORT'] || 587,
      :authentication => 'plain',
      enable_starttls_auto: true  
    }

    # Control this from the environment, but use the same port passed from the command line
    if Rails.env.development? || Rails.env.test?
      config.action_mailer.default_url_options = {
        :host => "#{ENV['ROOT_DOMAIN']}:#{defined?(Rails::Server) ? Rails::Server.new.options[:Port] : 3000}"
      }
    else
      config.action_mailer.default_url_options = {
        :host => ENV['ROOT_DOMAIN']
      }
    end
  end
end
