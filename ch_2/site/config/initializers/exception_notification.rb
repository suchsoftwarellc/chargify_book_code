FtwLawnCare::Application.configure do
  if ENV['SEND_ERROR_NOTIFICATIONS']
    config.middleware.use ExceptionNotification::Rack,
      :email => {
        :email_prefix         => "[#{Rails.env}] FtwLawnCare Error:  ",
        :sender_address       => %{"notifier" <notifier@railsstack.com>},
        :exception_recipients => ENV['ERROR_NOTIFICATION_RECIPIENT'],
        :delivery_method      => :smtp,
        :smtp_settings        => {
          :user_name            => ENV['SMTP_USERNAME'],
          :password             => ENV['SMTP_PASSWORD'],
          :domain               => ENV['SMTP_DOMAIN'],
          :address              => ENV['SMTP_ADDRESS'],
          :port                 => 587,
          :authentication       => :plain,
          :enable_starttls_auto => true
        }
    }
  end
end
