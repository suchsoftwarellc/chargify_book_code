FtwLawnCare::Application.routes.draw do
  devise_for :admins, :controllers => { :omniauth_callbacks => "admins/omniauth_callbacks" }
  devise_scope :admin do
    get '/admin/sign_in',  :to => 'admins/home#index', :as => :new_admin_session
    get '/admin/sign_out', :to => 'devise/sessions#destroy', :as => :destroy_admin_session
  end
  devise_for :users

  namespace :admins do
    get '/',          :to => 'home#index'
    get '/dashboard', :to => 'home#index'
    get '/queue',     :to => 'queue#index'

    resources :admins, :only => [:index, :show]
    resources :logs,   :only => :index
    resources :users do
      post 'confirm',             :on => :member
      post 'impersonate',         :on => :member
      post 'resend_confirmation', :on => :member
      post 'reset_password',      :on => :member
    end
  end

  namespace :chargify do
    get '/callbacks/signup',         :to => 'callbacks#signup'
    get '/callbacks/account_update', :to => 'callbacks#account_update'
  end

  root :to => "home#index"
  get '/terms', :to => 'home#terms'
  get '/privacy', :to => 'home#privacy'
  get '/goodbye_world', :to => 'application#goodbye_world'
end
