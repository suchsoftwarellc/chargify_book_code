custom_date_formats = {
  :email => "%A %B %e, %Y",
  :admin_date => "%d %b %Y %H:%M:%S %Z"
}
Date::DATE_FORMATS.merge!(custom_date_formats)
Time::DATE_FORMATS.merge!(custom_date_formats)
