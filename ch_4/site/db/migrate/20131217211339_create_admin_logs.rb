class CreateAdminLogs < ActiveRecord::Migration
  def change
    create_table :admin_logs do |t|
      t.integer :admin_id
      t.string  :action_type
      t.text    :description

      t.timestamps
    end
  end
end
