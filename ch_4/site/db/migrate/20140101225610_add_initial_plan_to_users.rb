class AddInitialPlanToUsers < ActiveRecord::Migration
  def change
    add_column :users, :initial_plan, :string
  end
end
