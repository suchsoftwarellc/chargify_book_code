class CreateSubscriptionsSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions_subscriptions do |t|
      t.integer :user_id
      t.string :level
      t.string :provider_id

      t.timestamps
    end
  end
end
