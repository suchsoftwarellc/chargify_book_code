class Admin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :timeoutable, :rememberable, :trackable, :omniauthable

  has_many :logs, :class_name => 'Admins::Log'
end
