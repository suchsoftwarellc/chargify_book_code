module Admins
  class Log < ActiveRecord::Base
    belongs_to :admin
    validates  :action_type, :presence => true

    # Constants for actions
    CONFIRMED_USER      = 'confirmed_user'      # Directly confirmed a user account rather than having the user do so via email
    LOGGED_IN           = 'logged_in'           # The Admin logged in
    FAILED_LOGIN        = 'failed_login'        # A failed login attempt to the admin portal
    MASQUERADED_AS      = 'masqueraded_as'      # An Admin pretended to be a user
    RESENT_CONFIRMATION = 'resent_confirmation' # An Admin resent the account confirmation instructions to a user
    RESET_PASSWORD      = 'reset_password'      # An Admin reset the user's password

    def self.confirmed_user(admin_id,user_id)
      user = User.find(user_id) rescue nil

      create(:admin_id => admin_id, :action_type => CONFIRMED_USER, :description => "Confirmed #{user_id}:#{user.email}")
    end

    def self.login(admin_id)
      create(:admin_id => admin_id, :action_type => LOGGED_IN)
    end

    def self.failed_login(email_address)
      create(:action_type => FAILED_LOGIN, :description => email_address)
    end

    def self.masquerade(admin_id,user_id)
      user = User.find(user_id) rescue nil

      create(:admin_id => admin_id, :action_type => MASQUERADED_AS, :description => "#{user_id} #{": #{user.email}" if user}")
    end

    def self.resent_confirmation(admin_id,user_id)
      user = User.find(user_id) rescue nil

      create(:admin_id => admin_id, :action_type => RESENT_CONFIRMATION, :description => "Resent confirmation instructions to #{user_id}#{":#{user.email}" if user}")
    end

    def self.reset_password(admin_id,user_id)
      user = User.find(user_id) rescue nil

      create(:admin_id => admin_id, :action_type => RESET_PASSWORD, :description => "Reset password on #{user_id}#{":#{user.email}" if user}")
    end
  end
end
