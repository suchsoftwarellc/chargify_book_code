class Admin::FindAdmin
  attr_reader :oauth_hash

  ALLOWABLE_ADMINS = [
    'ethan@bigohstudios.com'
  ]

  def initialize(oauth_hash,current_admin)
    @oauth_hash    = oauth_hash
    @current_admin = current_admin
  end

  def execute
    Admin.where(:email => oauth_hash.info[:email]).first
  end
end
