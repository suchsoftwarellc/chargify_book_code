class RegistrationsController < Devise::RegistrationsController
  def new; super; end
  def edit; super; end
  def update; super; end
  def destroy; super; end
  def cancel; super; end

  def create
    super
    if resource.persisted?
      resource.initial_plan = params[:user][:initial_plan]
      resource.save
    end
  end
end
