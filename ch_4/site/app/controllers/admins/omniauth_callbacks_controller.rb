module Admins
  class OmniauthCallbacksController < Devise::OmniauthCallbacksController
    skip_before_filter :store_location
  
    def google_oauth2
      # You need to implement the method below in your model (e.g. app/models/user.rb)
      command = Admin::FindAdmin.new(request.env['omniauth.auth'], current_admin)
      admin = command.execute
      
      if admin
        Log.login(admin.id)
        flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Google"
        sign_in_and_redirect admin, :event => :authentication
      else
        Log.failed_login(request.env['omniauth.auth'].info[:email])
        session["devise.google_data"] = request.env["omniauth.auth"]
        render :text => 'You cannot become an admin'
      end
    end
  end
end
