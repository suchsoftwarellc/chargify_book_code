module Admins
  class AdminsController < AdminsRootController
    def index
      @admins = Admin.order(:email).page params[:page]
    end

    def show
      @admin = Admin.find(params[:id])
      @logs  = @admin.logs.order('created_at DESC').page params[:log_page]
    end
  end
end
