module HomeHelper 
  def hosted_page_path(product)
    ENV["#{product.upcase}_SIGNUP_HOSTED_PAGE"]
  end
end
